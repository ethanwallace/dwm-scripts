# Ethan's DWM Startup Scripts

These are my startup scripts for DWM. They are to be placed in .xinitrc
**before** calling `exec dwm 2> ~/.dwm.log`, and should be called in the
following format:

`/path/to/file/script.sh &`

## About `statusbar.sh`

*Requires:* `xsetroot`, `alsa-utils`, and `acpi`

This script creates a statusbar through `xsetroot`. This bar contains the date
and time, the current volume (via `amixer`), the current wi-fi the system is
connected to plus the strength of the connection, and the battery charge
precentage and status (i.e., Charging/Discharging)

## About `wallpaper.sh`

*Requires:* `feh`

This script uses `feh` to set the system's wallpaper on startup

## About `screen.sh`

*Requires:* `slock`, `xautolock`

This script activates `slock` after 5 minutes of inactivity (via `xautolock`).
