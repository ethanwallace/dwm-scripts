#!/bin/sh

# Activates slock after 15 minutes of inactivity
#
# If you want the screen to lock on laptop-lid close then you have to put the
# following in your /etc/acpi/handler.sh under the "close)" button/lid case
# Your path to slock may be different.
#
# su -c - [your username] /usr/local/bin/slock

$(xautolock -time 15 -locker slock)
