#!/bin/sh

# This file contains the shell commands for the DWM status bar

get_date() {
    DATE=$(date +"%A %B %e %Y %r")
    echo $DATE
}

get_battery() {

    BATTERY=$(acpi | awk -F, '{print substr($2, 2, length($2)-2)}')
    STATUS=$(acpi | awk -F, '{print substr($1, 12, length($1))}')
    
    printf "%d%% [%s]" $BATTERY $STATUS
}

get_network() {
    
    NETWORK=$(wpa_cli list_networks | grep CURRENT | awk '{print $2}')
    QUALITY=$(cat /proc/net/wireless | grep wls1 | awk '{printf(substr($3, 0, 2))}')
    STRENGTH=$(echo "scale=2; ($QUALITY/70)*100" | bc -l)
   
    if [ -n $NETWORK ]; then
	printf "No Connection"
    else
	printf "%s [%3d%%]" $NETWORK $STRENGTH 2> /dev/null
    fi
}

get_volume() {
    
    VOLUME=$(amixer -M sget 'Master' | tail -1 | grep -o '[0-9]\+' | sed -n '2p')
    STATUS=$(amixer -M sget 'Master' | tail -1 | awk {'print $6'})

    if [ $VOLUME -eq '0' ]; then 
	printf "Volume: [-M-]"
    elif [ $STATUS = '[off]' ]; then
	printf "Volume: [-M-]"
    else
	printf "Volume: [%s%%]" $VOLUME
    fi
}

while true; do
	xsetroot -name "$(get_date) | $(get_volume) | $(get_network) | $(get_battery)"
    sleep 1s
done
